import pandas as pd
import numpy as np


#defininbg display options

pd.set_option('display.max_rows', 50)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 100)



# %% Defining the paths

# this is the path for the ISDB resukts file (topX)
isdb_results_path = "/Volumes/COMMON FASIE-FATHO/Chinedu/Combrataceae_POS/New_Params/FBMN_Combretum_MN/FBMN_Combretum_MN_results_top50.out"


# this is the path for the DNP or OPENNPDB datatable file
metadata_path = "~/190602_DNP_TAXcof_CF.csv"


#Path to weighed annotation result of ISDB
output_weighed_ISDB_path = "/Volumes/COMMON FASIE-FATHO/Chinedu/Combrataceae_POS/New_Params/FBMN_Combretum_MN/FBMN_Combretum_MN_results_top1_repond_classy_Pythoned.out"



# %% Loading the files

dt_isdb_results = pd.read_csv(isdb_results_path,
                                   sep='\t', error_bad_lines=False, low_memory=False)
dt_metadata = pd.read_csv(metadata_path,
                                      sep=',', error_bad_lines=False, low_memory=False)

# %%

dt_isdb_results.info()
# %%
# We keep only the relevant fields

dt_isdb_results = dt_isdb_results[['cluster index', 'Spectral_Score_DNP', 'InChIKey_DNP']]
# %%

# And drop the NAN entries

dt_isdb_results.dropna(inplace = True) 




# %% Here we want to split the unlnown numbers of results (in the score and IK columns) into multiple columns

dt_isdb_results = dt_isdb_results.join(dt_isdb_results['Spectral_Score_DNP'].str.split('|', expand=True).add_prefix('Spectral_Score_DNP_')).join(dt_isdb_results['InChIKey_DNP'].str.split('|', expand=True).add_prefix('InChIKey_DNP_'))


# %%
# Now we melt the whole stuff. For this we use the wide to long function https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.wide_to_long.html


dt_isdb_results = pd.wide_to_long(dt_isdb_results, ["Spectral_Score_DNP_", "InChIKey_DNP_"], i="cluster index", j="Rank_ISDB")

# %% 
# Here we add 1 to all ranks in order to have a coherent ranking numerotation (be careful !!! just run once .might be a bit dangerous to write like this)

dt_isdb_results.reset_index(inplace=True)
dt_isdb_results.set_index('cluster index', inplace=True)
dt_isdb_results['Rank_ISDB'] = dt_isdb_results['Rank_ISDB'] + 1


# %% 
# Some cleaning 

colstodrop = ['Spectral_Score_DNP',
'InChIKey_DNP']

dt_isdb_results.drop(colstodrop,  axis=1, inplace = True)


dt_isdb_results.rename(columns = {'Spectral_Score_DNP_':'Spectral_Score_ISDB',
                            'InChIKey_DNP_':'InChIKey_ISDB',
} , inplace = True)



# %% 
# Joining the DNP metadata

# we start by outputing the SIK for the ISDB output

dt_isdb_results['SIK_ISDB'] = dt_isdb_results.InChIKey_ISDB.str.split("-", expand=True)[0]


dt_isdb_results.reset_index(inplace=True)
# now we merge with the DNP metadata after selection of our columns of interest


cols_to_use = ['CRC_Number_DNP', 'IK_DNP', 'InChI_DNP',
       'Molecule_Name_DNP', 'Molecule_Formula_DNP', 
       'Accurate_Mass_DNP', 'Short_IK_DNP', 'Kingdom_cof_DNP', 'Phylum_cof_DNP',
       'Class_cof_DNP', 'Order_cof_DNP', 'Family_cof_DNP', 'Genus_cof_DNP',
       'Species_cof_DNP', 'Subspecies_cof_DNP', 'ClassyFy_Status_DNP',
       'Kingdom_cf_DNP', 'Superclass_cf_DNP', 'Class_cf_DNP',
       'Subclass_cf_DNP', 'Parent_Level_1_cf_DNP', 'Parent_Level_2_cf_DNP',
       'Parent_Level_3_cf_DNP', 'Parent_Level_4_cf_DNP',
       'Parent_Level_5_cf_DNP', 'Parent_Level_6_cf_DNP']


dt_isdb_results = pd.merge(left=dt_isdb_results, right=dt_metadata[cols_to_use], left_on='SIK_ISDB', right_on='Short_IK_DNP', how='inner')

dt_isdb_results.sort_values(by=['cluster index', 'Rank_ISDB'], inplace = True)
# %%

# Here we will add three columns (even for the simple repond this way it will be close to the multiple species repond)
# these line will need to be defined as function arguments 

#dt_isdb_results[dt_isdb_results['Genus_cof_DNP'].str.contains('Combretum', na = False)]


species_bio = 'Combretum caffrum'
genus_bio = 'Combretum'
family_bio = 'Combretaceae'
order_bio = 'Myrtales'
class_bio = 'Magnoliopsida'
phylum_bio = 'Tracheophyta'
kingdom_bio = 'Plantae'


dt_isdb_results['species_bio'] = species_bio
dt_isdb_results['genus_bio'] = genus_bio
dt_isdb_results['family_bio'] = family_bio
dt_isdb_results['order_bio'] = order_bio
dt_isdb_results['class_bio'] = class_bio
dt_isdb_results['phylum_bio'] = phylum_bio
dt_isdb_results['kingdom_bio'] = kingdom_bio



# %%

# Now we ouptut a 
# 
dt_isdb_results[dt_isdb_results.species_bio == dt_isdb_results.Species_cof_DNP]
# %%
